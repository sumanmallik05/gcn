# GCN
Graph Covolutional Network C implementation

1) Clone the repository:
`git clone git@bitbucket.org:sumanmallik05/gcn.git`

2) CD to the repo directory:
`cd GCN`

3)`module load cuda`
`module load cmake/3.21.3`

4) Build the executable code:
`./build`

5) Run the GCN:
`sbatch run_test cora # citeseer, reddit`
