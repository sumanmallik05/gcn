#include "header.h"

int main(int argc, char const *argv[]) {
	if ((argc != 2) || ((strcmp(argv[1], "cora") != 0) && (strcmp(argv[1], "citeseer") != 0) && (strcmp(argv[1], "reddit") != 0))) {
		printf("ERROR: usage \"%s [cora|citeseer|reddit]\"\n", argv[0]);
		return -1;
	}
	GCN_t GCN_c = GCN_parser((char*)argv[1]);
	feature_t feature_c;

	GCN_t device_GCN_c;
	
	int features_size = GCN_c.feature_c.feature_num*GCN_c.feature_c.node_num*sizeof(float);
	int indexes_size = (GCN_c.spec_c.nodes + 1)*sizeof(int);
	int neighbours_size = GCN_c.spec_c.edges*sizeof(int);
	int biasses_size = GCN_c.l1_parameter_c.out_feature_num*sizeof(float);
	int weights_size = GCN_c.l1_parameter_c.out_feature_num*GCN_c.l1_parameter_c.in_feature_num*sizeof(float);

	//Copying from Host to device
	device_GCN_c = GCN_c;
        	
	device_GCN_c.feature_c.feature_num = GCN_c.feature_c.feature_num;
	device_GCN_c.feature_c.node_num = GCN_c.feature_c.node_num;

	cudaMalloc((void**)&device_GCN_c.feature_c.features, features_size);
	cudaMalloc((void**)&device_GCN_c.graph_c.indexes, indexes_size);
	cudaMalloc((void**)&device_GCN_c.graph_c.neighbours, neighbours_size);

	cudaMalloc((void**)&device_GCN_c.l1_parameter_c.biasses, biasses_size);
        cudaMalloc((void**)&device_GCN_c.l1_parameter_c.weights, weights_size);

	cudaMalloc((void**)&device_GCN_c.l2_parameter_c.biasses, biasses_size);
        cudaMalloc((void**)&device_GCN_c.l2_parameter_c.weights, weights_size);

	cudaMemcpy(device_GCN_c.feature_c.features, GCN_c.feature_c.features, features_size, cudaMemcpyHostToDevice);
	cudaMemcpy(device_GCN_c.graph_c.indexes, GCN_c.graph_c.indexes, indexes_size, cudaMemcpyHostToDevice);
	cudaMemcpy(device_GCN_c.graph_c.neighbours, GCN_c.graph_c.neighbours, neighbours_size, cudaMemcpyHostToDevice);

	cudaMemcpy(device_GCN_c.l1_parameter_c.biasses, GCN_c.l1_parameter_c.biasses, biasses_size, cudaMemcpyHostToDevice);
        cudaMemcpy(device_GCN_c.l1_parameter_c.weights, GCN_c.l1_parameter_c.weights, weights_size, cudaMemcpyHostToDevice);

	cudaMemcpy(device_GCN_c.l2_parameter_c.biasses, GCN_c.l2_parameter_c.biasses, biasses_size, cudaMemcpyHostToDevice);
        cudaMemcpy(device_GCN_c.l2_parameter_c.weights, GCN_c.l2_parameter_c.weights, weights_size, cudaMemcpyHostToDevice);

	feature_c = aggregation(device_GCN_c.graph_c, device_GCN_c.feature_c);
//	feature_c = aggregation(GCN_c.graph_c, GCN_c.feature_c);
//	feature_c = combination(feature_c, GCN_c.l1_parameter_c, true);
	feature_c = combination(feature_c, device_GCN_c.l1_parameter_c, true);


	feature_c = aggregation(device_GCN_c.graph_c, feature_c);
//	feature_c = aggregation(GCN_c.graph_c, feature_c);
//	feature_c = combination(feature_c, GCN_c.l2_parameter_c, false);
	feature_c = combination(feature_c, device_GCN_c.l2_parameter_c, false);
	cudaDeviceSynchronize();
	analyzer(feature_c, GCN_c.label_c);

	return 0;
}
