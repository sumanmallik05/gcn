#include "header.h"
#define tile_width 32
__global__ void aggregation_kernel(graph_t graph_c, feature_t in_feature_c, float *out_features) {

	int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;

	if((col<in_feature_c.node_num) && (row<in_feature_c.feature_num)) {
	float out_ft=0;
	
		//for (int i = 0; i < in_feature_c.feature_num * in_feature_c.node_num; ++i) {
                	//out_features = 0;
       		//}
		for (int j = graph_c.indexes[col]; j < graph_c.indexes[col + 1]; ++j) {
        		out_ft += in_feature_c.features[row*in_feature_c.node_num + graph_c.neighbours[j]];
        	}
		//__syncthreads();
		out_features[row*in_feature_c.node_num+col] = out_ft;
               	out_features[row*in_feature_c.node_num + col] /= ((float)graph_c.indexes[col + 1] - graph_c.indexes[col]);
	}
	//__syncthreads();
}


feature_t aggregation (graph_t graph_c, feature_t in_feature_c) {
	int i, k, j;
	feature_t out_feature_c;

	printf("AGGREGATION: A[%d][%d] * X[%d][%d] = X'[%d][%d]\n", in_feature_c.node_num, in_feature_c.node_num, in_feature_c.node_num, in_feature_c.feature_num, in_feature_c.node_num, in_feature_c.feature_num);

	out_feature_c.feature_num = in_feature_c.feature_num;
	out_feature_c.node_num = in_feature_c.node_num;
	out_feature_c.features = (float*) malloc (in_feature_c.feature_num * in_feature_c.node_num * sizeof(float));

	float* out_features;
	int features_size = in_feature_c.feature_num*in_feature_c.node_num*sizeof(float);
	cudaMalloc((void**)&out_features, features_size);


	dim3 DimGrid(ceil(in_feature_c.feature_num/16.0),ceil(in_feature_c.node_num/16.0));       
        dim3 DimBlock(16,16);

        aggregation_kernel<<<DimGrid, DimBlock>>>(graph_c, in_feature_c, out_features);

	cudaMemcpy(out_feature_c.features, out_features, features_size, cudaMemcpyDeviceToHost);



/*	for (i = 0; i < in_feature_c.feature_num * in_feature_c.node_num; ++i) {
//		out_feature_c.features[i] = (float*) malloc (in_feature_c.node_num * sizeof(float));
//		for (j = 0; j < in_feature_c.node_num; ++j) {
			out_feature_c.features[i] = 0;
//		}
	}

	for (i = 0; i < in_feature_c.node_num; ++i) {
//		printf("\r%.2f%% Completed!", (float)i * 100.00 / (float)in_feature_c.node_num);
	    fflush(stdout);
		for (k = 0; k < in_feature_c.feature_num; ++k) {
			for (j = graph_c.indexes[i]; j < graph_c.indexes[i + 1]; ++j) {
				out_feature_c.features[k*in_feature_c.node_num+i] += in_feature_c.features[k*in_feature_c.node_num + graph_c.neighbours[j]];
			}
			out_feature_c.features[k*in_feature_c.node_num + i] /= ((float)graph_c.indexes[i + 1] - graph_c.indexes[i]);
		}
	}
*/

	return out_feature_c;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

__global__ void combination_kernel (feature_t in_feature_c, parameter_t parameter_c, bool relu, float* out_features) {

	int tx = threadIdx.x;
	int ty = threadIdx.y;
	int col = blockIdx.x * blockDim.x + tx;
        int row = blockIdx.y * blockDim.y + ty;

	float cvalue=0;
	
	__shared__ float features[tile_width][tile_width];
	__shared__ float weights[tile_width][tile_width];

	for( int numtile = 0; numtile< ceil(in_feature_c.node_num/float(tile_width)); ++numtile) {
		
		if((row<in_feature_c.feature_num)&&(col<in_feature_c.node_num)) {
			features[row][col] = in_feature_c.features[row*in_feature_c.node_num + tile_width*numtile + tx];
		}
		else 
			features[ty][tx] = 0;
		if((row<parameter_c.out_feature_num)&&(col<parameter_c.in_feature_num)) {
			weights[row][col] = parameter_c.weights[(numtile*tile_width+ty)*in_feature_c.node_num + col];
		}
		else
			weights[ty][tx] = 0;
		__syncthreads();
		
		for(int k=0; k<tile_width; ++k) {
			cvalue += features[ty][k]*weights[k][tx];
		}
		__syncthreads();

		if((row<parameter_c.out_feature_num)&&(col<in_feature_c.node_num)){
			out_features[row*parameter_c.out_feature_num + col] = cvalue;
		}

		if(relu){
			out_features[row*parameter_c.out_feature_num + col] = MAX(0.00000, out_features[row*parameter_c.out_feature_num]);
		}

//		if((col<in_feature_c.node_num) && (row<parameter_c.out_feature_num)) {
// 	       		float out_ft=0;

//			out_features[row*in_feature_c.node_num + col] = parameter_c.biasses[row];
//        		for (int k = 0; k < parameter_c.in_feature_num; ++k) {
//        			out_features[row*in_feature_c.node_num + col] += in_feature_c.features[k*in_feature_c.node_num + col] * parameter_c.weights[k*parameter_c.out_feature_num +row];
//       			}
//      			if(relu)
//       				out_features[row*in_feature_c.node_num + col] = MAX(0.00000, out_features[row*in_feature_c.node_num + col]);
//		}
	}
}

feature_t combination (feature_t in_feature_c, parameter_t parameter_c, bool relu) {
	int i, j, k;
	feature_t out_feature_c;

	if (in_feature_c.feature_num != parameter_c.in_feature_num) {
    	printf("ERROR: Incompatible number of features in feature (%d) and parameter (%d) objects!\n", in_feature_c.feature_num, parameter_c.in_feature_num);
    	exit(-1);
	}

	printf("COMBINATION: X'[%d][%d] * W[%d][%d] = X[%d][%d]\n", in_feature_c.node_num, in_feature_c.feature_num, parameter_c.in_feature_num, parameter_c.out_feature_num, in_feature_c.node_num, parameter_c.out_feature_num);

	out_feature_c.node_num = in_feature_c.node_num;
	out_feature_c.feature_num = parameter_c.out_feature_num;
	out_feature_c.features = (float*) malloc (parameter_c.out_feature_num * in_feature_c.node_num * sizeof(float));

	float* out_features;
        int features_size = in_feature_c.feature_num*in_feature_c.node_num*sizeof(float);
        cudaMalloc((void**)&out_features, features_size);

        dim3 DimGrid(ceil(in_feature_c.node_num/1.0*tile_width),ceil(parameter_c.out_feature_num/1.0*tile_width),1);        
        dim3 DimBlock(tile_width,tile_width,1);

        combination_kernel<<<DimGrid, DimBlock>>>(in_feature_c, parameter_c, relu, out_features);
        cudaMemcpy(out_feature_c.features, out_features, features_size, cudaMemcpyDeviceToHost);

	return out_feature_c;
}

void analyzer (feature_t feature_c, label_t label_c) {
	int i, j;
	int correct_num = 0;

	for (i = 0; i < feature_c.node_num; ++i) {
		float max_feature = feature_c.features[0*feature_c.node_num + i];
		int max_idx = 0;
		for (j = 1; j < feature_c.feature_num; ++j) {
			if(feature_c.features[j*feature_c.node_num + i] > max_feature) {
				max_feature = feature_c.features[j*feature_c.node_num + i];
				max_idx = j;
			}
		}
		if (max_idx == label_c[i]) {
			correct_num++;
		}
	}
	
	printf("Accuracy: %.2f%%\n", (float)correct_num * 100.00 / (float)feature_c.node_num);
}
